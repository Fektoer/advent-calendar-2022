#![feature(array_windows)]
fn main() {
    let count = include_str!("../input.txt")
        .lines()
        .map(|line| line.parse().unwrap())
        .collect::<Vec<u16>>()
        .array_windows()
        .filter(|[a, _, _c, d]| a < d)
        .count();
    print!("{count}")
}
