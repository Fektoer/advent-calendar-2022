#![feature(array_windows)]
fn main() {
    let instructions = include_str!("../input.txt")
        .lines()
        .map(|l| l.split_once(" ").unwrap());

    print!("{:?}", instructions)
}
